﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MyBankApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SavingsAccount account1 = new SavingsAccount("111-001", "Bob", 2000.00, 0.02);
        SavingsAccount account2 = new SavingsAccount("111-002", "Mary", 5000.00, 0.02);
        
        public MainPage()
        {
            this.InitializeComponent();
            account1Txt.Text = account1.ToString();
            account2Txt.Text = account2.ToString();
        }

        private void account1DepositBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            account1.Deposit(1000);
            account1Txt.Text = account1.Balance.ToString();
        }

        private void account1WithdrawBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            if (account1.Withdraw(1000) == false)
            {
                account1Txt.Text = "Insufficient Fund";
            }
            else
            {
                account1Txt.Text = account1.Balance.ToString();
            }
        }

        private void account1InterestBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            account1Txt.Text = string.Format("The Interest is: {0}", account1.CalculateInterest());
        }

        private void account2DepositBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            account2.Deposit(1000);
            account2Txt.Text = account2.Balance.ToString();
        }

        private void account2WithdrawBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            
            if (account2.Withdraw(1000)==false)
            {
                account2Txt.Text = "Insufficient Fund";
            }
            else
            {
                account2Txt.Text = account2.Balance.ToString();
            }
        }

        private void account2InterestBtn_DoubleTapped(object sender, RoutedEventArgs e)
        {
            account2Txt.Text = string.Format("The Interest is: {0}", account2.CalculateInterest());
        }
    }
}

