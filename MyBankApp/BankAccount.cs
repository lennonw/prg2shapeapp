﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp
{
    class BankAccount
    {
        private string accNo;
        public string AccountNo
        {
            get { return accNo; }
            set { accNo = value; }
        }
        private string accName;
        public string AccName
        {
            get { return accName; }
            set { accName = value; }
        }
        private double balance;
        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }
        public BankAccount()
        {

        }
        public BankAccount(string num, string name, double b)
        {
            AccountNo = num;
            AccName = name;
            Balance = b;
        }
        public void Deposit(double amount)
        {
            balance = balance + amount;
        }
        public bool Withdraw (double amount)
        {
            if (amount <= balance)
            {
                balance -= amount;
                return true;
            }
            else
            {
                return false;
            }
        }
        public double EnquireBalance()
        {
            return balance;
        }

        public override string ToString()
        {
            return $"Balance : {balance}";
        }



    }
}
