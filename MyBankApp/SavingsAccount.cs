﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp
{
    class SavingsAccount : BankAccount
    {
        private double rate;
        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }
        public SavingsAccount(): base ()
        {
        }
        public SavingsAccount(string num, string name, double b, double d) : base(num, name ,b)
        {
            base.AccountNo = num;
            base.AccName = name;
            base.Balance = b;
            Rate = d;
        }

        public double CalculateInterest()
        {
            return Balance * Rate;
        }
        public override string ToString()
        {
            return string.Format($"Account Name: {base.AccName} Account No.: {base.AccountNo} Balance : {base.Balance} Interest Rate : {Rate}");
        }
    }
}
